(defproject codamic/beleth "0.1.0-SNAPSHOT"
  :description "Secret keeper"
  :url "https://gitlab.com/Codamic/beleth"
  :license {:name "GPLv3"
            :url "https://www.gnu.org/licenses/gpl-3.0.en.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [codamic/hellhound "1.0.0-SNAPSHOT"]
                 [org.clojure/tools.cli "0.3.7"]]

  :main ^:skip-aot beleth.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
