;; Beleth
;; Copyright (C) 2018  Codamic

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
(ns beleth.components.cli
  (:require
    [hellhound.component :as com]
    [hellhound.streams   :as streams]))


(defn print-help
  []
  (println "Usage:")
  (println "\thelp\t\tShow Help.")
  (println "\tversion\t\tShow Version."))


(defn print-version
  []
  (println "0.1.0"))


(defn parse-args
  [args]
  (let [command (or (keyword (first args)) :help)]
    {:command command
     :args    (rest args)}))


(defn ->command
  [parsed-options]
  {:commands [:config :show]})



(defn produce-command
  [args]
  (fn [component]
    (let [out     (com/output component)
          data    (parse-args args)]
      (cond
        (= (:command data) :help)    (print-help)
        (= (:command data) :version) (print-version)
        :else (streams/put! out (->command data))))))

1
(defn factory
  [name args]
  {:hellhound.component/start-fn (fn [this _] this)
   :hellhound.component/stop-fn  identity
   :hellhound.component/name name
   :hellhound.component/fn   (produce-command args)})
