;; Beleth
;; Copyright (C) 2018  Codamic

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
(ns beleth.components.config
  (:require
   [clojure.java.io :as io]
   [hellhound.component :refer [deftransform]]
   [hellhound.streams   :as streams]))


(defn get-home
  []
  (str (System/getenv "HOME")))


(defn config-dir
  []
  (io/file (get-home) ".beleth"))


(deftransform loader
  [this v]
  (assoc v :config {}))


(defn unknown-command
  [command]
  {:msg (format "Unknown command '%s'." command)
   :type :error})

(defn show-config
  []
  {:type :success
   :msg "Beleth local configuration:"
   :result [["name" "Sameer Rahmani"]]})

(defn set-config
  [v]
  {:type :success
   :msg "Done."})

(defn apply-commend
  [commands v]
  (let [command (first commands)]
    (cond
      (= command :show) (show-config)
      (= command :set)  (set-config v)
      :else (unknown-command))))

(deftransform command
  [this v]
  (let [commands (:commands commands)]
    (apply-commend (rest commands) v)))
