;; Beleth
;; Copyright (C) 2018  Codamic

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
(ns beleth.system
  (:require
   [beleth.components :as components]
   [beleth.components.stdout :as stdout]
   [beleth.components.config :as config]
   [beleth.components.key :as k]))


(defn command?
  [value k]
  (= (:command value) k))


(defn factory
  [args]
  {:components [(components/cli-factory ::cli args)
                stdout/printer
                k/command
                config/loader]

   :workflow [[::cli :beleth.components.stdout/printer]
              [::cli :beleth.components.config/loader]
              [:beleth.components.config/loader #(command? % :config) :beleth.components.config/command]]

   :execution {:mode :single-thread}})
